#!/usr/bin/env python
import unittest
from game import Game, MultiGame
import pickle 
import unittest


class HattariLogicTest(unittest.TestCase):

    def test_game_init_(self):
        game=Game(3)
        self.assertEqual(len(game.players),3)
        self.assertEqual(len(game.suspects),3)
        self.assertEqual(game.stage,1)
        for i in range (0,3):
            self.assertFalse(game.victim==game.alibis[i])
            self.assertFalse(game.victim==game.suspects[i].value)

    def test_Multigame_init_(self):
        game=MultiGame(3)
        self.assertEqual(len(game.players),3)
        self.assertEqual(len(game.suspects),3)
        self.assertEqual(game.stages,0)
        for i in range (0,3):
            self.assertFalse(game.victim==game.alibis[i])
            self.assertFalse(game.victim==game.suspects[i].value)

    def test_rotateAlibis(self):
        # Checks if the rotating of alibis work
        game=Game(4)
        temp=[]
        for i in range (0,4):
            temp.append(game.alibis[i])
        game.stage=2
        self.assertTrue(game.rotateAlibis())
        self.assertEqual(temp[0],game.alibis[3])
        self.assertEqual(temp[1],game.alibis[0])
        self.assertEqual(temp[2],game.alibis[1])
        self.assertEqual(temp[3],game.alibis[2])

    def test_rotateAlibisMulti(self):
        # Checks if the rotating of alibis work
        game=MultiGame(4)
        temp=[]
        for i in range (0,4):
            temp.append(game.alibis[i])
        game.stages=1
        self.assertTrue(game.rotateAlibis())
        self.assertEqual(temp[0],game.alibis[3])
        self.assertEqual(temp[1],game.alibis[0])
        self.assertEqual(temp[2],game.alibis[1])
        self.assertEqual(temp[3],game.alibis[2])

    def test_swapSuspect(self):
        # Swap suspect test
        game=Game(4)
        victim=game.victim
        suspect=game.suspects[0].value
        game.currTurn=2
        self.assertFalse(game.swapSuspect(1))
        game.currTurn=1
        self.assertTrue(game.swapSuspect(1))
        self.assertEqual(victim,game.suspects[0].value)
        self.assertEqual(suspect,game.victim)

    def test_swapSuspect_Multi(self):
        # Swap suspect test
        game=MultiGame(4)
        victim=game.victim
        suspect=game.suspects[0].value
        game.currTurn=2
        self.assertFalse(game.swapSuspect(1))
        game.currTurn=1
        self.assertTrue(game.swapSuspect(1))
        self.assertEqual(victim,game.suspects[0].value)
        self.assertEqual(suspect,game.victim)


    # def test_judge(self):
    #     # testing the judge method, different test with no 5, with a 5, with a 5 as the lowest number
    #     # with a blank as one of the suspects
    #     game=Game(4)
    #     game.suspects[0].value=2
    #     game.suspects[1].value=3
    #     game.suspects[2].value=4
    #     self.assertEqual(game.judge(),4)
    #     game.suspects[0].value=2
    #     game.suspects[1].value=3
    #     game.suspects[2].value=5
    #     self.assertEqual(game.judge(),2)
    #     game.suspects[0].value=7
    #     game.suspects[1].value=8
    #     game.suspects[2].value=5
    #     self.assertEqual(game.judge(),5)
    #     game.suspects[0].value=7
    #     game.suspects[1].value=5
    #     game.suspects[2].value=0
    #     self.assertEqual(game.judge(),5)

    def test_returnTokens(self):
        # tesing the returnToken method
        game=Game(4)
        game.stage=7
        game.suspects[0].value=2
        game.suspects[1].value=3
        game.suspects[2].value=4
        game.suspects[0].accTokens.append(1)
        game.suspects[0].accTokens.append(2)
        game.suspects[1].accTokens.append(3)
        game.suspects[1].accTokens.append(4)
        game.returnTokens()
        self.assertEqual(game.players[1].blackTokens,2)
        self.assertEqual(game.players[3].blackTokens,2)
        self.assertEqual(game.players[0].blackTokens,0)
        self.assertEqual(game.players[2].blackTokens,0)

    def test_returnTokens_Multi(self):
        # tesing the returnToken method
        game=MultiGame(4)
        game.stage=7
        game.suspects[0].value=2
        game.suspects[1].value=3
        game.suspects[2].value=4
        game.suspects[0].accTokens.append(1)
        game.suspects[0].accTokens.append(2)
        game.suspects[1].accTokens.append(3)
        game.suspects[1].accTokens.append(4)
        game.returnTokens()
        self.assertEqual(game.players[1].blackTokens,2)
        self.assertEqual(game.players[3].blackTokens,2)
        self.assertEqual(game.players[0].blackTokens,0)
        self.assertEqual(game.players[2].blackTokens,0)


    def test_gameOverCheck4(self):
        # testing gameOverCheck with 4 players and different conditions
        game=Game(4)
        self.assertFalse(game.gameOverCheck())
        game.players[0].blackTokens=10
        self.assertTrue(game.gameOverCheck())
        game.newGame(4)
        game.players[0].accTokens=0 # Checking if 0 accTokens leads to game over
        self.assertTrue(game.gameOverCheck())
        game.newGame(4)
        game.players[0].blackTokens=3
        game.players[1].accTokens=0
        self.assertTrue(game.gameOverCheck())
        self.assertEqual(len(game.loserIds),2)

    def test_gameOverCheck4_Multi(self):
        # testing gameOverCheck with 4 players and different conditions
        game=MultiGame(4)
        self.assertFalse(game.gameOverCheck())
        game.players[0].blackTokens=10
        self.assertTrue(game.gameOverCheck())
        game.newGame(4)
        game.players[0].accTokens=0 # Checking if 0 accTokens leads to game over
        self.assertTrue(game.gameOverCheck())
        game.newGame(4)
        game.players[0].blackTokens=3
        game.players[1].accTokens=0
        self.assertTrue(game.gameOverCheck())
        self.assertEqual(len(game.loserIds),2)

    def test_gameOverCheck3(self):
        # testing gameOverCheck with 3 players and different conditions
        game=Game(3)
        self.assertFalse(game.gameOverCheck())
        game.players[0].blackTokens=10
        self.assertTrue(game.gameOverCheck())
        game.newGame(3)
        game.players[0].accTokens=0 # Checking if 0 accTokens leads to game over
        self.assertTrue(game.gameOverCheck())
        game.newGame(3)
        game.players[0].blackTokens=3
        game.players[1].accTokens=0
        self.assertTrue(game.gameOverCheck())
        self.assertEqual(len(game.loserIds),2)

    def test_gameOverCheck3_Multi(self):
        # testing gameOverCheck with 3 players and different conditions
        game=MultiGame(3)
        self.assertFalse(game.gameOverCheck())
        game.players[0].blackTokens=10
        self.assertTrue(game.gameOverCheck())
        game.newGame(3)
        game.players[0].accTokens=0 # Checking if 0 accTokens leads to game over
        self.assertTrue(game.gameOverCheck())
        game.newGame(3)
        game.players[0].blackTokens=3
        game.players[1].accTokens=0
        self.assertTrue(game.gameOverCheck())
        self.assertEqual(len(game.loserIds),2)


if __name__ == '__main__':
    unittest.main()