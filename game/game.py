from random import shuffle
from .player import Player
from .suspect import Suspect


quotes= ['The player is surely bluffing', 'The player is never bluffing',
         'The player is really smart', 'The player is always surprising me',
         'The player is a genius I will never win']


class Game:
    def __init__(self, numPlayers):
        self.newGame(numPlayers)
        self.hideBoard()
        self.board['gameId'] = 0

    # The functions below are public.

    # Start a new game.
    def newGame(self, numPlayers):
        self.numPlayers = numPlayers
        self.players = []
        for id in range(1, numPlayers+1):
            self.players.append(Player(id))
        self.observerId = 0
        self.stage = 1
        self.loserIds = []
        self.newRound()

    # Start a new round
    def newRound(self):
        if self.stage != 1 or len(self.loserIds) > 0:
            return False
        deal = self.__dealCards(self.numPlayers)
        self.alibis = []
        for id in range(1, self.numPlayers+1):
            self.alibis.append(deal.pop(0))
        self.suspects = []
        for i in range(0, 3):
            self.suspects.append(Suspect(deal.pop(0)))
        self.victim = deal.pop(0)
        self.murderer = None
        self.players[self.observerId-1].observer = False
        self.observerId = self.observerId % self.numPlayers + 1
        self.players[self.observerId-1].observer = True
        self.currTurn = self.observerId
        self.hideBoard()
        self.swapped = False
        self.stageHistory = []
        self.board['info']=["no info" for player in self.players]
        return True

    # Hide all values of cards on the board.
    def hideBoard(self):
        self.board = {}
        self.board['players'] = [player.json() for player in self.players]
        self.board['alibis'] = [-1 for i in range(len(self.alibis))]
        self.board['victim'] = -1
        self.board['suspects'] = [suspect.jsonHidden() for suspect in self.suspects]
        self.board['stage'] = self.stage
        self.board['currTurn'] = self.currTurn
        return True

    # Show the result of one round.
    def showResult(self):
        if self.stage != 6:
            return False
        self.murderer = self.__judge()
        for suspect in self.suspects:
            if suspect.value == self.murderer:
                suspect.murderer = True
        self.board = {}
        self.board['players'] = [player.json() for player in self.players]
        self.board['alibis'] = [alibi for alibi in self.alibis]
        self.board['victim'] = self.victim
        self.board['suspects'] = [suspect.json() for suspect in self.suspects]
        self.board['stage'] = self.stage
        self.board['currTurn'] = self.currTurn
        self.__nextStage()
        return True

    # Check alibi by players in turn.
    def checkAlibi(self, playerId):
        if self.stage not in [1, 3] or playerId != self.currTurn:
            return False
        self.board['alibis'][self.currTurn-1] = self.alibis[self.currTurn-1]
        self.__nextStage()
        return True

    # Rotate alibis anti-clockwise.
    def rotateAlibis(self):
        if self.stage != 2:
            return False
        self.alibis = self.alibis[1:] + self.alibis[0:1]
        self.__nextStage()
        return True

    # Choose two suspects to check by observer.
    def chooseSuspects(self, suspectId1, suspectId2):
        if suspectId1 not in [1, 2, 3] or suspectId2 not in [1, 2, 3] \
          or self.stage != 4 or not self.players[self.currTurn-1].observer:
            return False
        self.board['suspects'][suspectId1-1] = self.suspects[suspectId1-1].json()
        self.board['suspects'][suspectId2-1] = self.suspects[suspectId2-1].json()
        suspectId3 = 1 + 2 + 3 - suspectId1 - suspectId2
        self.suspects[suspectId3-1].unseenToken = True
        self.board['suspects'][suspectId3-1]['unseenToken'] = True
        self.__nextStage()
        return True

    # Let non-observer players to check suspects haven't been placed token last turn.
    def checkSuspects(self):
        if self.stage != 4 or self.players[self.currTurn-1].observer:
            return False
        for suspectId in [i for i in range(1, 4) if i != self.suspectPlacedToken]:
            self.board['suspects'][suspectId-1] = self.suspects[suspectId-1].json()
        self.__nextStage()
        return True

    # Return tokens according to the rule.
    def returnTokens(self):
        if self.stage != 7:
            return False
        for suspect in self.suspects:
            if suspect.value != self.murderer:
                if suspect.accTokens:
                    self.players[suspect.accTokens[-1]-1].blackTokens += len(suspect.accTokens)
            else:
                for id in suspect.accTokens:
                    self.players[id-1].accTokens += 1
            suspect.accTokens = []
        self.board['players'] = [player.json() for player in self.players]
        self.__nextStage()
        return True

    # Place token on suspect.
    def placeToken(self, suspectId):
        if self.stage == 5 and suspectId in [1, 2, 3]:
            self.players[self.currTurn-1].accTokens = self.players[self.currTurn-1].accTokens - 1
            self.suspects[suspectId-1].accTokens.append(self.currTurn)
            self.suspectPlacedToken = suspectId
            self.__nextStage()
            return True
        return False

    # Observer can swap one suspect with the victim.
    def swapSuspect(self, suspectId):
        if not self.swapped and self.players[self.currTurn-1].observer and suspectId in [1, 2, 3]:
            self.suspects[suspectId-1].value, self.victim = self.victim, self.suspects[suspectId-1].value
            self.suspects[suspectId-1].swapToken = True
            self.board['suspects'][suspectId-1]['value'], self.board['victim'] = \
                self.board['victim'], self.board['suspects'][suspectId-1]['value']
            self.board['suspects'][suspectId-1]['swapToken'] = True
            self.swapped = True
            return True
        return False

    # Check whether the game is over or not.
    def gameOverCheck(self):
        for player in self.players:
            if (player.accTokens + player.blackTokens) > 7 or player.accTokens <= 0:
                self.loserIds.append(player.id)
        if len(self.loserIds) > 0:
            return True
        return False

    def pdkbBox(self,players,actions):
        self.board['info'] = ['Must explicitly request mental state']*len(self.players)
    
    def mentalState(self, players, actions):
        try:
            self.board['info'] = []

            print "Actions: %s" % str(actions)
            players[0].actions = actions
            print "Solving..."
            players[0].solve()
            print "Saving mental state..."
            RMLs = str(players[0].final_state.relevant_output()).split('\n')
            RMLs = filter(lambda x: 'neighbour' not in x, RMLs)
            RMLs = filter(lambda x: 'P' != x.strip()[0], RMLs)
            return '\n'.join(RMLs)
            #self.board['info'].append('\n'.join(RMLs))
            #for i in range(len(self.players)-1):
            #    self.board['info'].append("Private mind!")

            #for p in players:
            #    p.actions = actions
            #    p.solve()
            #    RMLs = str(p.final_state.relevant_output()).split('\n')
            #    RMLs = filter(lambda x: 'neighbour' not in x, RMLs)
            #    RMLs = filter(lambda x: 'P' != x.strip()[0], RMLs)
            #    self.board['info'].append('\n'.join(RMLs))
        except Exception as e:
            return "Error computing mental state: %s" % str(e)
            self.board['info'] = [str(e)]*len(self.players)

    def quoteBox(self):
        shuffle(quotes)
        self.board['info']=[" " for p in self.players]
        for i in range(len(self.players)):
            self.board['info'][i]=quotes[i+1]


    # The functions below are private.

    # Shuffle cards for different numbers of players.
    def __dealCards(self, numPlayers):
        deal = [0, 2, 3, 4, 5, 6, 7, 8]
        if numPlayers < 4:
            deal.remove(2)
        if numPlayers < 3:
            deal.remove(8)
        shuffle(deal)
        return deal

    # Find out the murderer.
    def __judge(self):
        suspectValues = [suspect.value for suspect in self.suspects if suspect.value > 0]
        minVal = min(suspectValues)
        maxVal = max(suspectValues)
        murderer = minVal if 5 in suspectValues else maxVal
        return murderer

    # Set the stage and currTurn to next stage.
    # For more info, please read "Stages System" in Architectural Design.
    def __nextStage(self):
        self.stageHistory.append((self.stage, self.currTurn))
        if self.stage in [1, 3]:
            self.__nextTurn()
            if (self.stage, self.currTurn) in self.stageHistory:
                self.stage += 1
                if len(self.players) == 2:
                    self.stage = 4
        elif self.stage in [2, 4, 6]:
            self.stage += 1
        elif self.stage == 5:
            self.__nextTurn()
            if (self.stage, self.currTurn) in self.stageHistory:
                self.stage += 1
            else:
                self.stage = 4
        elif self.stage == 7:
            self.stage = 1
        self.board['stage'] = self.stage
        self.board['currTurn'] = self.currTurn

    # Set currTurn to next turn.
    # For more info, please read "Stages System" in Architectural Design.
    def __nextTurn(self):
        self.currTurn = self.currTurn % self.numPlayers + 1
