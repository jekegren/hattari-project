class Suspect:
    def __init__(self, value, swapToken=False, unseenToken=False, murderer=False):
        self.value = value
        self.accTokens = []
        self.swapToken = swapToken
        self.unseenToken = unseenToken
        self.murderer = murderer

    def json(self):
        return self.__dict__

    def jsonHidden(self):
        jsonHidden = dict(self.__dict__)    
        jsonHidden['value'] = -1
        return jsonHidden