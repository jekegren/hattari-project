class Player:
    def __init__(self, id, observer=False):
        self.id = id
        self.username=""
        self.accTokens = 5
        self.blackTokens = 0
        self.observer = observer

    def json(self):
        return self.__dict__
