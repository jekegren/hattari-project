# Hattari

Hattari is a game of bluffing and deduction, and the player who ends up with the fewest accusation markers wins the game.
[[PLAY IT HERE!](http://hattari.haz.ca)]

Hattari is original designed by [Jun Sasaki](http://oinkgms.com).

This is a university project. It is for academic purpose only.

###Contributions

The main developers are:

* [Johan Gunnarsson](https://bitbucket.org/jekegren)

* [Shiyi Wang](https://bitbucket.org/shiywang)

This project is supervised by:

* [Christian Muise](https://bitbucket.org/haz)

* [Tim Miller](https://bitbucket.org/gibber_blot)

###Screenshot
![hattari_screenshot.jpg](https://bitbucket.org/repo/oj8nLp/images/1127324687-hattari_screenshot.jpg)