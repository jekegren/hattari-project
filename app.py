
import os, traceback, pickle, copy

from flask import Flask, render_template, request, jsonify
from flask.ext.bootstrap import Bootstrap
from flask.ext.sqlalchemy import SQLAlchemy as BaseSQLAlchemy

from game import Game, MultiGame
from pdkb import planner

bootstrap = Bootstrap()

class SQLAlchemy(BaseSQLAlchemy):
    def apply_driver_hacks(self, app, info, options):
        if 'isolation_level' not in options:
            options['isolation_level'] = 'READ UNCOMMITTED'
        return super(SQLAlchemy, self).apply_driver_hacks(app, info, options)

app = Flask(__name__, static_url_path='/static')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db' # used for local testing
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
bootstrap.init_app(app)
db = SQLAlchemy(app)
app.debug = True

@app.route("/create_database")
def db_create():
    db.create_all()
    pdkbs = pdkbDb(['']*4)
    db.session.add(pdkbs)
    db.session.commit()
    return 'Database created!'

@app.route("/drop_database")
def db_drop():
    db.drop_all()
    return 'Database dropped!'

@app.route("/setup_player/<int:pid>")
def setup_player(pid):
    if pid not in [1,2,3,4]:
        return "Invalid player id %s (must be 1-4)" % str(pid)

    try:
        player = planner.compile('abcd'[pid-1])

        if 1 == pid:
            pdkbDb.query.with_for_update().get(1).p1 = pickle.dumps(player)
        elif 2 == pid:
            pdkbDb.query.with_for_update().get(1).p2 = pickle.dumps(player)
        elif 3 == pid:
            pdkbDb.query.with_for_update().get(1).p3 = pickle.dumps(player)
        elif 4 == pid:
            pdkbDb.query.with_for_update().get(1).p4 = pickle.dumps(player)

        db.session.commit()
    except Exception as e:
        print e

    return "Player %s setup!" % str(pid)

class pdkbDb(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    p1 = db.Column(db.PickleType, unique=False)
    p2 = db.Column(db.PickleType, unique=False)
    p3 = db.Column(db.PickleType, unique=False)
    p4 = db.Column(db.PickleType, unique=False)

    def __init__(self, players):
        self.p1 = players[0]
        self.p2 = players[1]
        self.p3 = players[2]
        self.p4 = players[3]

# The database table with an unique id generated automatically, a pickled game object, four booleans one for each player,
# and one Integer
class gameDb(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    gameObj = db.Column(db.PickleType, unique=False)
    numPlayersLeft = db.Column(db.Integer, unique=False)
    ready1 = db.Column(db.Boolean, unique=False)
    ready2 = db.Column(db.Boolean, unique=False)
    ready3 = db.Column(db.Boolean, unique=False)
    ready4 = db.Column(db.Boolean, unique=False)
    numReadyNext = db.Column(db.Integer, unique=False)
    actionSequence = db.Column(db.String, unique=False)
    def __init__(self,gameObj,numPlayersLeft):
        self.gameObj = gameObj
        self.numPlayersLeft=numPlayersLeft
        self.numReadyNext=0
        self.ready1=False
        self.ready2=False
        self.ready3=False
        self.ready4=False
        self.actionSequence=''

def getPlayers():
    pdkbs = pdkbDb.query.with_for_update().get(1)
    return [pickle.loads(pdkbs.p1), None, None, None]
    #return [pickle.loads(pdkbs.p1), pickle.loads(pdkbs.p2), pickle.loads(pdkbs.p3), pickle.loads(pdkbs.p4)]

def getActions(gameId):
    return gameDb.query.with_for_update().get(gameId).actionSequence[1:].split(',')

# Get the pickled gameObject from the database and return it unpickled
def getGameObj(gameId):
    pickledGame=gameDb.query.with_for_update().get(gameId).gameObj
    return pickle.loads(pickledGame)

# Updates the database with a new pickled game
def updateDb(gameId,game,move=None):
    print "Move: %s" % str(move)
    pickledGame=pickle.dumps(game)
    gameDb.query.with_for_update().get(gameId).gameObj=pickledGame
    if move == 'reset':
        gameDb.query.with_for_update().get(gameId).actionSequence=''
    elif move is not None:
        gameDb.query.with_for_update().get(gameId).actionSequence+=','+move
    db.session.commit()

# Returns the number of players left
def getNumPlayersLeft(gameId):
    return gameDb.query.with_for_update().get(gameId).numPlayersLeft

# Deletes the game from the database
def deleteGame(gameId):
    db.session.delete(gameDb.query.get(gameId))
    db.session.commit()

# Updates the specific players boolean variable
def updateReady(gameId,playerId):
    if (playerId==1):
        gameDb.query.get(gameId).ready1=True
    elif (playerId==2):
        gameDb.query.get(gameId).ready2=True
    elif (playerId==3):
        gameDb.query.get(gameId).ready3=True
    elif (playerId==4):
        gameDb.query.get(gameId).ready4=True
    db.session.commit()

# Counts number of players that are ready
def numReady(gameId):
    numready=0
    if (gameDb.query.get(gameId).ready1==True):
        numready += 1
    if (gameDb.query.get(gameId).ready2==True):
        numready += 1
    if (gameDb.query.get(gameId).ready3==True):
        numready += 1
    if (gameDb.query.get(gameId).ready4==True):
        numready += 1
    return numready

# Updates a player's ready variable
@app.route("/game/multi/ready/<int:gameId>&<int:playerId>")
def ready(gameId, playerId):
    updateReady(gameId,playerId)
    game=getGameObj(gameId)
    if(game.stages in [0, 2]):
        game.board['highlight'].append(playerId)
        updateDb(gameId,game)
    return jsonify({}), 200

# Update that a player is ready for the next round
@app.route("/game/multi/readynextround/<int:gameId>&<int:playerId>")
def readyNextRound(gameId, playerId):
    gameDb.query.with_for_update().get(gameId).numReadyNext += 1
    db.session.commit()
    game=getGameObj(gameId)
    game.board['highlight'].append(playerId)
    updateDb(gameId,game)
    return jsonify({}), 200

# Going to a next stage all the ready variables are set to false
def nextStage(gameId):
    gameDb.query.with_for_update().get(gameId).ready1=False
    gameDb.query.with_for_update().get(gameId).ready2=False
    gameDb.query.with_for_update().get(gameId).ready3=False
    gameDb.query.with_for_update().get(gameId).ready4=False
    db.session.commit()

# Returns all the rooms with free spots
@app.route("/getrooms")
def getRooms():
    roomsDb=gameDb.query.all()
    rooms=[]
    for r in roomsDb:
        if (r.numPlayersLeft>0):
            rooms.append(r.id)
    return jsonify(rooms=rooms)

# Start page
@app.route("/")
def index():
    return render_template('index.html')

# Local game page
@app.route("/game")
def game():
    return render_template('game.html')

# Online game page
@app.route("/multigame")
def multigame():
    return render_template('multigame.html', getRooms=getRooms)

# Rule page
@app.route("/rule")
def rule():
    return render_template('rule.html')

@app.route("/demo")
def demo():
    return render_template('demo.html')

@app.route("/game/mentalstate/<int:gameId>")
def mentalState(gameId):
    try:
        game=getGameObj(gameId)
        kb = game.mentalState(getPlayers(), getActions(gameId))
        print "Computed metal state:\n%s" % kb
    except Exception as e:
        print e
        kb = "Error: %s" % str(e)
    return jsonify(state=kb)

# Start a new game with "numPlayers" Players.
@app.route("/game/start/<int:numPlayers>")
def startGame(numPlayers):
    if numPlayers not in range(1, 5):
        e = "The number of players is out of range."
        return jsonify(message=e)
    game = Game(numPlayers)
    pickledGame=pickle.dumps(game)
    dbGame=gameDb(pickledGame,0)
    db.session.add(dbGame)
    db.session.commit()
    gameId=dbGame.id
    game.board['gameId']=gameId
    if numPlayers == 2:
        game.stage=3
    game.quoteBox()
    updateDb(gameId,game)
    return jsonify(game.board)

# Start a new round.
@app.route("/game/newround/<int:gameId>")
def newRound(gameId):
    game=getGameObj(gameId)
    success = game.newRound()
    if success:
        updateDb(gameId,game,'reset')
    else:
        e = "Starting new round is not allowed at current stage. Stage:"+str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# Let the player in current turn check the alibi.
@app.route("/game/checkalibi/<int:playerId>/<int:gameId>") #/<int:gameidn>")
def checkAlibi(playerId, gameId):
    game=getGameObj(gameId)
    success = game.checkAlibi(playerId)
    if success:
        updateDb(gameId,game,"check_%s_c%s" % ('abcd'[playerId-1], str(game.alibis[playerId-1]).replace('0', '1')))
    else:
        e = "Checking alibi is not allowed at current stage. Stage:" + str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# Hide all values of cards on the board.
@app.route("/game/hideboard/<int:gameId>")
def hideBoard(gameId):
    game=getGameObj(gameId)
    game.hideBoard()
    updateDb(gameId,game)
    game.quoteBox()
    return jsonify(game.board)

# Rotate alibis.
@app.route("/game/rotatealibis/<int:gameId>")
def rotateAlibis(gameId):
    game=getGameObj(gameId)
    success = game.rotateAlibis()
    if success:
        updateDb(gameId,game,'rotate')
    else:
        e = "Rotating alibis is not allowed at current stage. Stage:"+str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# Observer chooses 2 suspects to check.
@app.route("/game/choosesuspects/<int:suspectId1>&<int:suspectId2>&<int:gameId>")
def chooseSuspects(suspectId1, suspectId2, gameId):
    game=getGameObj(gameId)
    curPlayer = 'abcd'[game.currTurn-1]
    success = game.chooseSuspects(suspectId1, suspectId2)
    if success:
        updateDb(gameId,game,"peek_%s_spot%d_c%s,peek_%s_spot%s_c%s" % \
                 (curPlayer, suspectId1, str(game.suspects[suspectId1-1].value).replace('0','1'),
                  curPlayer, suspectId2, str(game.suspects[suspectId2-1].value).replace('0','1')))
    else:
        e = "Checking suspects is not allowed at current stage. Stage:"+str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# Non-observer players check the suspects haven't been placed token at last turn.
@app.route("/game/checksuspects/<int:gameId>")
def checkSuspects(gameId):
    game=getGameObj(gameId)
    curPlayer = 'abcd'[game.currTurn-1]
    success = game.checkSuspects()
    if success:
        sids = [i for i in range(1, 4) if i != game.suspectPlacedToken]
        updateDb(gameId,game,"peek_%s_spot%d_c%s,peek_%s_spot%s_c%s" % \
                 (curPlayer, sids[0], str(game.suspects[sids[0]-1].value).replace('0','1'),
                  curPlayer, sids[1], str(game.suspects[sids[1]-1].value).replace('0','1')))
    else:
        e = "Checking suspects is not allowed at current stage. Stage:"+str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# Observer swap one suspect with the victim.
@app.route("/game/swapsuspect/<int:suspectId>&<int:gameId>")
def swapSuspect(suspectId,gameId):
    game=getGameObj(gameId)
    success = game.swapSuspect(suspectId)
    updateDb(gameId,game)
    game.quoteBox()
    if not success:
        e = "Swapping suspect and victim is not allowed at current stage. Stage:"+str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# Player in current turn places a token on one suspect.
@app.route("/game/placetoken/<int:suspectId>&<int:gameId>")
def placeToken(suspectId,gameId):
    game=getGameObj(gameId)
    success = game.placeToken(suspectId)
    updateDb(gameId,game)
    game.quoteBox()
    if not success:
        e = "Placing token is not allowed at current stage. Stage:"+str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# The system shows the result at the end of one round.
@app.route("/game/showresult/<int:gameId>")
def showResult(gameId):
    game=getGameObj(gameId)
    success = game.showResult()
    updateDb(gameId,game)
    game.quoteBox()
    if not success:
        e = "Showing result is not allowed at current stage. Stage:"+str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# Return tokens at the end of one round.
@app.route("/game/returntokens/<int:gameId>")
def returnTokens(gameId):
    game=getGameObj(gameId)
    success = game.returnTokens()
    updateDb(gameId,game)
    game.quoteBox()
    if not success:
        e = "Returning tokens is not allowed at current stage. Stage:"+str(game.stage)
        return jsonify(message=e)
    return jsonify(game.board)

# Check whether a game is over.
@app.route("/game/gameovercheck/<int:gameId>")
def gameOverCheck(gameId):
    game=getGameObj(gameId)
    gameover = game.gameOverCheck()
    updateDb(gameId,game)
    game.quoteBox()
    if gameover:
        msg = "The game is over."
        deleteGame(gameId)
    else:
        msg = "The game is not over yet."
    return jsonify(gameover=gameover, message=msg, loserIds = game.loserIds)

# -------- Multiplayer Version of the game ------

# Update a game with the poll function
@app.route("/game/multi/update/<int:gameId>")
def updateMulti(gameId):
    updated = False
    game=getGameObj(gameId)
    if gameDb.query.get(gameId).numReadyNext == game.numPlayers:
        newRoundMulti(gameId)
    game=getGameObj(gameId)
    game.board['numPlayersLeft'] = getNumPlayersLeft(gameId)
    if numReady(gameId) == game.numPlayers:
        game.stages += 1
        if game.numPlayers == 2:
            game.stages = 3
        nextStage(gameId)
        game.board['highlight'] = []
    if game.stages in [3, 4, 5, 6]:
        game.board['highlight'] = [game.currTurn]
    game.board['stage'] =game.stages
    updateDb(gameId,game)
    game.quoteBox()
    return jsonify(game.board, ready = gameDb.query.get(gameId).numReadyNext)

# join a game with game Id and username as inputs
@app.route("/game/multi/join/<int:gameId>/<username>")
def joinRoomMulti(gameId,username):
    if gameDb.query.get(gameId).numPlayersLeft == 0:
        e = "The game is already full."
        return jsonify(message=e)
    gameDb.query.with_for_update().get(gameId).numPlayersLeft -= 1
    db.session.commit()
    game=getGameObj(gameId)
    playerId=game.numPlayers-gameDb.query.get(gameId).numPlayersLeft
    game.board['player']=game.numPlayers-gameDb.query.get(gameId).numPlayersLeft
    game.players[playerId-1].username=username
    game.board['usernames'][playerId-1]=username
    updateDb(gameId,game)
    return jsonify(game.board)

# Start a new multiplayer game with "numPlayers" Players.
@app.route("/game/multi/start/<int:numPlayers>/<username>")
def startMultiGame(numPlayers,username):
    if numPlayers not in range(1, 5):
        e = "The number of players is out of range."
        return jsonify(message=e)
    game = MultiGame(numPlayers)
    pickledGame=pickle.dumps(game)
    dbGame=gameDb(pickledGame,numPlayers-1)
    db.session.add(dbGame)
    db.session.commit()
    gameId=dbGame.id
    game.board['gameId']=gameId
    game.board['player']=1
    game.players[0].username=username
    game.board['usernames'][0]=username
    updateDb(gameId,game)
    return jsonify(game.board)


# Start a new round.
@app.route("/game/multi/newround/<int:gameId>")
def newRoundMulti(gameId):
    game=getGameObj(gameId)
    game.returnTokens()
    success = game.newRound()
    if not success:
        e = "Starting new round is not allowed at current stage."
        return jsonify(message=e)
    gameDb.query.get(gameId).numReadyNext = 0
    updateDb(gameId,game)
    db.session.commit()
    return jsonify(game.board)

# Let the player in current turn check the alibi.
@app.route("/game/multi/checkalibi/<int:playerId>/<int:gameId>") #/<int:gameidn>")
def checkAlibiMulti(playerId, gameId):
    game=getGameObj(gameId)
    game.quoteBox()
    success = game.checkAlibi(playerId)
    if not success:
        e = "Checking alibi is not allowed at current stage."
        return jsonify(message=e)
    return jsonify(game.board)

# Hide all values of cards on the board.
@app.route("/game/multi/hideboard/<int:gameId>")
def hideBoardMulti(gameId):
    game=getGameObj(gameId)
    game.hideBoard()
    updateDb(gameId,game)
    return jsonify(game.board)

# Rotate alibis.
@app.route("/game/multi/rotatealibis/<int:gameId>")
def rotateAlibisMulti(gameId):
    game=getGameObj(gameId)
    success = game.rotateAlibis()
    updateDb(gameId,game)
    if not success:
        e = "Rotating alibis is not allowed at current stage."
        return jsonify(message=e)
    return jsonify(game.board)


# Observer chooses 2 suspects to check.
@app.route("/game/multi/choosesuspects/<int:suspectId1>&<int:suspectId2>&<int:gameId>")
def chooseSuspectsMulti(suspectId1, suspectId2,gameId):
    game=getGameObj(gameId)
    copiedBoard=copy.deepcopy(game.board)
    board = game.chooseSuspects(copiedBoard,suspectId1, suspectId2)
    suspectId3 = 1 + 2 + 3 - suspectId1 - suspectId2
    game.board['suspects'][suspectId3-1]['unseenToken'] = True
    success=True
    updateDb(gameId,game)
    if not success:
        e = "Checking suspects is not allowed at current stage."
        return jsonify(message=e)
    return jsonify(board)

# Non-observer players check the suspects haven't been placed token at last turn.
@app.route("/game/multi/checksuspects/<int:gameId>")
def checkSuspectsMulti(gameId):
    game=getGameObj(gameId)
    copiedBoard=copy.deepcopy(game.board)
    board = game.checkSuspects(copiedBoard)
    success=True
    if not success:
        e = "Checking suspects is not allowed at current stage."
        return jsonify(message=e)
    return jsonify(board)


# Observer swap one suspect with the victim.
@app.route("/game/multi/swapsuspect/<int:suspectId>&<int:gameId>")
def swapSuspectMulti(suspectId,gameId):
    game=getGameObj(gameId)
    success = game.swapSuspect(suspectId)
    updateDb(gameId,game)
    if not success:
        e = "Swapping suspect and victim is not allowed at current stage."
        return jsonify(message=e)
    return jsonify(game.board)

# Player in current turn places a token on one suspect.
@app.route("/game/multi/placetoken/<int:suspectId>&<int:gameId>&<int:playerId>")
def placeTokenMulti(suspectId,gameId,playerId):
    game=getGameObj(gameId)
    success = game.placeToken(suspectId, playerId)
    if not success:
        e = "Placing token is not allowed at current stage."
        return jsonify(message=e)
    game.stages += 1
    if game.stages == game.numPlayers + 3:
        game.stages=7
    updateDb(gameId,game)
    return jsonify(game.board)

# The system shows the result at the end of one round.
@app.route("/game/multi/showresult/<int:gameId>")
def showResultMulti(gameId):
    game=getGameObj(gameId)
    success = game.showResult()
    updateDb(gameId,game)
    if not success:
        e = "Showing result is not allowed at current stage."
        return jsonify(message=e)
    return jsonify(game.board)

# Return tokens at the end of one round.
@app.route("/game/multi/returntokens/<int:gameId>")
def returnTokensMulti(gameId):
    game=getGameObj(gameId)
    success = game.returnTokens()
    updateDb(gameId,game)
    if not success:
        e = "Returning tokens is not allowed at current stage."
        return jsonify(message=e)
    return jsonify(game.board)

# Check whether a game is over.
@app.route("/game/multi/gameovercheck/<int:gameId>")
def gameOverCheckMulti(gameId):
    game=getGameObj(gameId)
    gameover = game.gameOverCheck()
    updateDb(gameId,game)
    if gameover:
        msg = "The game is over."
    else:
        msg = "The game is not over yet."
    return jsonify(game.board, gameover=gameover, message=msg, loserIds = game.loserIds)

@app.route("/game/multi/endgame/<int:gameId>&<int:playerId>")
def endGame(gameId,playerId):
    game=getGameObj(gameId)
    updateReady(gameId,playerId)
    if numReady(gameId) == game.numPlayers:
        deleteGame(gameId)
        e = "Game Deleted"
        return jsonify(message=e)
    return jsonify(game.board)

if __name__ == "__main__":
    app.run(debug = True)
    port = int(os.environ.get("PORT", 8080))
    host = os.getenv("IP", '0.0.0.0')
    app.run(host=host, port=port)
