var suspectsChosen = [];
var userName="";
var playerId=0;
var numPlayersLeft=null;
var stage=0;
var internalStage=0;
var checked = false;
var ready;
var renderCheck = false;
var resultCheck = false;
var gameStartNotice = false;
var checkedAlibi= true;
var nextAlibiClicked;
var placeTokenClicked;

$(function() {

	// Action for "Start New Game" button.
	$('#start').click(function() {
		hideActions();
		hideDescription();
		$('div.gameboard').css('opacity', '0.0');
		$('#username').show();
		$('#nextStartGame').show();
	});

	// Action after you typed in your username
	$('#nextStartGame').click(function() {
		hideActions();
		hideDescription();
		$('div.gameboard').css('opacity', '0.0');
		userName=$('#user').val();
		howManyPlayers();
		hideGameBoard();
	});
	// Action for join room button
	$('#joinroom').click(function() {
		hideActions();
		hideDescription();
		$('div.gameboard').css('opacity', '0.0');
		$('#username').show();
		$('#nextJoinRoom').show();	
	});
	// Action after typed in your username
	$('#nextJoinRoom').click(function() {
		hideActions();
		hideDescription();
		$('div.gameboard').css('opacity', '0.0');
		userName=$('#user').val();
		showRooms();
	});

	// Action for join room button
	$('body').on('click', '.btn.btn-success.action.join', function() {joinRoom($(this).attr("value"))});

	// Action for "Rotate Alibis" button.
	$('#rotateAlibis').click(function() {rotateAlibis()});

	// Action for "Place Token" button.
	$('#placeToken').click(function() {placeToken()});

	// Action for "Show Result" button.
	$('#showResult').click(function() {showResult()});

	// Action for "Return Tokens" button.
	$('#returnTokens').click(function() {returnTokens()});

	// Action for "Next Round" button.
	$('#nextRound').click(function() {readyNextRound()});

	// Action for "Yes" button.
	$('#yes').click(function() {yes()});

	// Action for "No" button.
	$('#no').click(function() {no()});

	// Action for "Swap Suspect" button.
	$('#swapSuspect').click(function() {swapSuspect()});

	$('#refreshRooms').click(function() {getRooms()});


	/* Action for 2-4 Players buttons. */
	for (var num = 2; num <= 4; num++) 
		$('#'+num).click(startGame(num));

	for (var id = 1; id <= 3; id++) {
		/* Action for green "Suspect" buttons. */
		$('#chooseSuspect'+id).click(chooseSuspectHandler(id, suspectsChosen));
		/* Action for blue "Suspect" buttons. */
		$('#removeSuspect'+id).click(removeSuspectHandler(id, suspectsChosen));
	}
})

/* Hide the description. */
function hideDescription() {
	$('#description').html('<br>');
}


/* Hide all actions buttons. */
function hideActions() {
	$('#username').hide();
	$('.btn.btn-success.action').hide();
}

/* Hide board. */
function hideBoard() {
	$.getJSON("/game/multi/hideboard/"+gameId, function(json) {renderBoard(json)});
}

/* Ask player how many players. */
function howManyPlayers() {
	$('#description').text("How many players?").hide().fadeIn();
	for (var i = 2; i <= 4; i++) $('#'+i).fadeIn();
}
// Display all the rooms with free spots
function getRooms() {
	$.getJSON("/getrooms", function(json) {
		html = ""
		for(var i = 0; i < json.rooms.length; i++) {
			html += "<tr>\
						<td>#" + json.rooms[i] + "</td>\
						<td>\
						<button type=\"button\" class=\"btn btn-success action join\" value=\"" + json.rooms[i] + "\">Join Room</button>\
						</td>\
						<td></td>\
					</tr>";
		}
		$("#tbody-rooms").html(html);
	});
}
/* A polling function that connects to the server every second, depening on what stage and interal stage it is in
* different functions and buttons will be called and displayed. */
function doPoll(){
    $.getJSON("/game/multi/update/"+gameId,function(json) {
    	numPlayersLeft=json['numPlayersLeft'];
		stage=json['stage'];
		ready=json.ready;
		hover(json);
		highLightPlayers(json);
    	if (numPlayersLeft>0){
    		hideActions();
    		renderBoard(json);
    	}
		else if ((stage==0 && internalStage==0 && !checked) || stage==2 && internalStage==2 && !checked )  {
			renderBoard(json);
			if (checkedAlibi) {
				checkedAlibi=false;
				$('#checkAlibi').off();
				$('#checkAlibi').on('click', function () {
					$('#checkAlibi').off();
					checkAlibi();
				});
			}
			$('#checkAlibi').fadeIn();
			if(!gameStartNotice) {
				toastr.success('Game start.');
				gameStartNotice = true;
			}
			if(renderCheck) {
				renderBoard(json);
				renderCheck = false;
			}
		}
		else if (stage==1 && internalStage==1){
			if(json.players[playerId-1].observer)
				rotateAlibis();
			else
				rotateAnimation(json.players.length);
		}
		else if (stage==3 && json.players[playerId-1].observer && internalStage == 3){
			internalStage +=1;
			$('#chooseSuspects').on('click', function () {
					$('#chooseSuspects').off();
					chooseSuspects();});
			$('#chooseSuspects').show();
			$('.btn.btn-success.choose-suspect').show();
			$('#description').text('Please choose 2 suspects to check.');
		}
		else if (stage==3) {
			renderBoard2(json);
		}
		else if (stage==4 && playerId==json['currTurn'] && internalStage ==3 ){
			internalStage +=1;
			renderBoard(json);
			$('#checkSuspects').on('click', function () {
					$('#checkSuspects').off();
					checkSuspects();});
			$('#checkSuspects').show();
		}
		else if (stage==4){
			renderBoard2(json);
		}
		else if (stage==5 && playerId==json['currTurn'] && internalStage ==3 ){
			renderBoard(json);
			$('#checkSuspects').on('click', function () {
					$('#checkSuspects').off();
					checkSuspects();});
			$('#checkSuspects').show();
			internalStage +=1;
		}
		else if (stage==5){
			renderBoard2(json);
		}
		else if (stage==6 && playerId==json['currTurn'] && internalStage ==3 ){
			renderBoard(json);
			$('#checkSuspects').on('click', function () {
					$('#checkSuspects').off();
					checkSuspects();});
			$('#checkSuspects').show();
			internalStage +=1;
		}
		else if (stage==6){
			renderBoard2(json);
		}
		else if (stage==7 && internalStage == 4) {
			showResult();
			setTimeout(function() {returnTokens()}, 2000);
			internalStage = 0;
		}
        setTimeout(doPoll,1000);
    });
}

// Start game with 'numPlayers' players.
function startGame(numPlayers) {
	return function() {
		$.getJSON("/game/multi/start/"+numPlayers+'/'+userName, function(json) {
			$('.player').css('display', 'none');
			$('.alibi').css('display', 'none');
			playerId=json.player
			renderBoard(json);
			hideActions();
			hideDescription();
			gameId=json.gameId;
			$('#title').text('Hattari Game Room #'+gameId)
			$('div.tokenTop').css('visibility', 'hidden');
			$('div.gameboard').css('opacity', '1.0');
			$('div.gameboard').hide().fadeIn(750);
			doPoll();
			resultCheck = false;
			//$('#checkAlibi2').hide();
			//$('#checkAlibi').show();
		});
	}
}

/* Action for "Check Alibi" button. */
function checkAlibi() {
	$.getJSON("/game/multi/checkalibi/"+playerId+'/'+gameId, function(json) {
		$('#alibi'+playerId).fadeOut(0, function() {
			renderBoard(json);
			$('#alibi'+playerId).hide().fadeIn(750);
			hideActions();
			$('#nextAlibi').on('click',function(){
				$('#nextAlibi').off();
				nextAlibi();});
			$('#nextAlibi').fadeIn();
			nextAlibiClicked = false;
			checked = true;
		});
	});
}

// joins a room and gets a player ID
function joinRoom(gameToJoin){
	$.getJSON("/game/multi/join/"+gameToJoin+'/'+userName, function(json) {
		$('.player').css('display', 'none');
		$('.alibi').css('display', 'none');
		gameId=json.gameId;
		$('#title').text('Hattari Game Room #'+gameId)
		renderBoard(json);
		hideActions();
		hideDescription();
		hideRooms();
		playerId=json.player
		doPoll();
		$('div.gameboard').css('opacity', '1.0');
		$('div.gameboard').fadeIn(750, 0.4);
	})
	.fail(function(json){
		toastr.warning('The room is full.');
	});
}

/* Action for "Next" button. */
function nextAlibi() {
	$.getJSON("/game/multi/hideboard/"+gameId, function(json) {
		$('#alibi'+playerId).fadeOut(750, function() {
			if(!nextAlibiClicked) {
				renderBoard(json);
				$('#alibi'+playerId).hide().fadeIn(750);
				hideActions();
				internalStage += 1;
				if(json.players.length == 2)
					internalStage = 3;
				$.getJSON("/game/multi/ready/"+gameId+"&"+playerId, function() {});
				nextAlibiClicked = true;
			}
		});
	});
}

/* Action for "Rotate Alibis" button. */
function rotateAlibis() {
	$.getJSON("/game/multi/rotatealibis/"+gameId, function(json) {
		numPlayers = json.players.length;
	    rotateAnimation(numPlayers);
		renderBoard(json);
		hideActions();
	});
}

// Rotating animation that moves the alibis around
function rotateAnimation(numPlayers) {
	$('#alibi1').animate({right: "-="+(242.5*(numPlayers-1))+"px"}, {queue: false, complete: function() {
		$('#alibi1').animate({right: "+="+(242.5*(numPlayers-1))+"px"}, 0);
	}});
	for(var id = 2; id <= numPlayers; id++) {
		$('#alibi'+id).animate({left: "-=242.5px"}, {queue: false, complete: function() {
			$(this).animate({left: "+=242.5px"}, 0);
		}});
	}
	internalStage=2;
	$.getJSON("/game/multi/ready/"+gameId+"&"+playerId);
	checked = false;
	checkedAlibi=true
}

/* Action for "Choose suspects" button. */
function chooseSuspects() {
	if (suspectsChosen.length != 2) {
		$('#chooseSuspects').on('click', function () {
					$('#chooseSuspects').off();
					chooseSuspects();});
		return toastr.warning('Please choose 2 suspects.'); }
	$.getJSON("/game/multi/choosesuspects/"+suspectsChosen[0]+'&'+suspectsChosen[1]+'&'+gameId, function(json) {
		$('#suspect'+suspectsChosen[0]+', #suspect'+suspectsChosen[1]).fadeOut(750, function() {
			renderBoard(json);
			clearSuspectsChosen();
			$('.btn.btn-primary.choose-suspect').hide();
			$('.btn.btn-success.choose-suspect').fadeIn();
			$('#description').text('Do you want to swap one suspect with the victim?').hide().fadeIn();
			hideActions();
			$('#yes').fadeIn();
			$('#no').fadeIn();
			clearSuspectsChosen();
			$('.btn.btn-primary.choose-suspect').hide();
			$('.btn.btn-success.choose-suspect').fadeIn();
		});
	});
}

/* Action for "Yes" button. */
function yes() {
	hideActions();
	clearSuspectsChosen();
	$('#swapSuspect').fadeIn();
	$('#description').text('Please choose 1 suspect to swap with the victim.').hide().fadeIn();
	$('.btn.btn-primary.choose-suspect').hide();
	$('.btn.btn-success.choose-suspect').fadeIn();
}

/* Action for "No" button. */
function no() {
	hideActions();
	clearSuspectsChosen();
	$('#placeToken').fadeIn();
	placeTokenClicked = false;
	$('#description').text('Please choose 1 suspect to place token.').hide().fadeIn();
	$('.btn.btn-primary.choose-suspect').hide();
	$('.btn.btn-success.choose-suspect').fadeIn();
}

/* Action for "Swap Suspect" button. */
function swapSuspect() {
	if (suspectsChosen. length != 1)
		return toastr.warning('Please choose 1 suspect.');
	$.getJSON("/game/multi/swapsuspect/"+suspectsChosen[0]+'&'+gameId, function(json) {
		$('#victim, #suspect'+suspectsChosen[0]).fadeOut(750, function() {
			renderBoard(json);
			$('#victim, #suspect'+suspectsChosen[0]).fadeIn(750);
			hideActions();
			clearSuspectsChosen();
			$('#placeToken').fadeIn();
			placeTokenClicked = false;
			$('#description').text('Please choose 1 suspect to place token.').hide().fadeIn();
			$('.btn.btn-primary.choose-suspect').hide();
			$('.btn.btn-success.choose-suspect').fadeIn();
		});
	});
}

/* Action for "Check suspects" button. */
function checkSuspects() {
	$.getJSON("/game/multi/checksuspects/"+gameId, function(json) {
		var s = [];
		for(var i = 1; i <= json.suspects.length; i++) {
			if(json.suspects[i-1].value >= 0)
				s.push(i);
		}
		$('#suspect'+s[0]+', #suspect'+s[1]).fadeOut(750, function() {
			renderBoard(json);
			hideActions();
			clearSuspectsChosen();
			$('#placeToken').fadeIn();
			placeTokenClicked = false;
			$('#description').text('Please choose 1 suspect to place token.').hide().fadeIn();
			$('.btn.btn-primary.choose-suspect').hide();
			$('.btn.btn-success.choose-suspect').fadeIn();
		});
	});
}

/* Action for "Place token" button. */
function placeToken() {
	if(placeTokenClicked)
		return null;
	if(suspectsChosen.length != 1)
		return toastr.warning('Please choose 1 suspect to place token.');
	$.getJSON("/game/multi/placetoken/"+suspectsChosen[0]+'&'+gameId+'&'+playerId, function(json) {
		var s = [];
		for(var i = 1; i <= json.suspects.length; i++) {
			if(json.suspects[i-1].value >= 0)
				s.push(i);
		}
		$('#suspect'+s[0]+', #suspect'+s[1])
		.fadeOut(750)
		.promise()
		.done(function() {
			renderBoard(json);
			hideActions();
			clearSuspectsChosen();
			hideDescription();
			hideBoard();
			$('.btn.btn-primary.choose-suspect').hide();
			$('.btn.btn-success.choose-suspect').hide();
		});
		checked = true;
		placeTokenClicked = true;
	});
}

/* Action for "Show result" button. */
function showResult() {
	$.getJSON("/game/multi/showresult/"+gameId, function(json) {
		$('#alibis, #victim, #suspects').fadeOut(750)
		.promise()
		.done(function() {
			for (var i = 0; i < 3; i++) {
				if(json.suspects[i].murderer) {
					$('#description').text('The murderer is ' + json.suspects[i].value).hide();
					$('#suspect'+(i+1)).css('color', 'red');
				}
			}
			renderBoard(json);
			$('#alibis, #victim, #suspects').show();
			hideActions();
		});
	});
}

/* Action for "Return Tokens" button. */
function returnTokens() {
	$.getJSON("/game/multi/returntokens/"+gameId, function(json) {
		gameOverCheck();
	});
}

// Checks whether the gameover is reached
function gameOverCheck() {
	$.getJSON("/game/multi/gameovercheck/"+gameId, function(json) {
		if (!json.gameover) {
			$('#nextRound').fadeIn();
			$('#description').fadeIn();
		}
		else {
			checkedAlibi=true;
			msg = 'GAME OVER. Loser:';
			for (var i = 0; i < json.loserIds.length; i++)
				msg += ' Player ' + (json.loserIds[i]);
			$('#description').text(msg).hide().fadeIn();
			$('div.gameboard').fadeTo(750, 0.4);
			$('#startNew').on('click', function () {
					$('#startNew').off();
				hideActions();
				hideDescription();
				$('div.gameboard').css('opacity', '0.0');
				$('#username').show();
				$('#nextStartGame').show();
				endGame();
				});
			$('#joinNewRoom').on('click', function () {
					$('#joinNewRoom').off();
				hideActions();
				hideDescription();
				$('div.gameboard').css('opacity', '0.0');
				$('#username').show();
				$('#nextJoinRoom').show();
				endGame();
						});
			$('#startNew, #joinNewRoom').fadeIn();
			gameStartNotice = false;
			checked = false;
		}
	});
}
// Starts a new round of the game
function nextRound() {
	$.getJSON("/game/multi/newround/"+gameId, function(json) {
		checkedAlibi=true;
		wtf="jjjjjj";
		renderBoard(json);
		hideActions();
		playerId = json.currTurn;
		$('div.suspect').css('color', '#ffffff');
		hideDescription();
		checked = true;
	});
}

function endGame() {
	$.getJSON("/game/multi/endgame/" + gameId + "&" + playerId);
}

/* Action for "Next Round" button. */
function readyNextRound() {
	$.getJSON("/game/multi/readynextround/"+gameId+"&"+playerId);
	checkedAlibi=true;
	$('#nextRound').hide();
	checked = false;
	hideDescription();
	$('div.suspect').css('color', '#ffffff');
	renderCheck = true;
}

/* Clear suspectsChosen list. */
function clearSuspectsChosen() {
	suspectsChosen.length = 0;
}

/* Action for green "Suspect" buttons. */
function chooseSuspectHandler(id, suspectsChosen) {
	return function() {
		suspectsChosen.push(id);
		$('#chooseSuspect'+id).hide();
		$('#removeSuspect'+id).show();
	}
}

/* Action for blue "Suspect" buttons. */
function removeSuspectHandler(id, suspectsChosen) {
	return function() {
		index = suspectsChosen.indexOf(id);
		if (index >= 0) {
			suspectsChosen.splice(index, 1);
			$('#chooseSuspect'+id).show();
			$('#removeSuspect'+id).hide();
		}
	}
}

// Info board that shows quotes about all the players
function hover(board) {
	for (var id=1; id <= 4; id++) {
		hovering(board,id)
	}
}
function hovering(board,id){
	$('#player'+id).click(function(){
		var hovertext=board.info[id-1]
		if(id==playerId)
			hovertext="I am the king of Hattari"
		$('#well').show();
		$('#well_text').text(hovertext);
});}


/* Render the board. */
function renderBoard(board) {
	for (var id = 1; id <= board.players.length; id++) {
		renderPlayer(id, board.players[id-1],board.usernames[id-1]);
		renderAlibi(id, board.alibis[id-1]);
	}
	renderVictim(board['victim'])
	for (var id = 1; id <= 3; id++) renderSuspect(id, board.suspects[id-1])
}
// Another board used for stages when opponents are looking at the suspects
function renderBoard2(board) {
	for (var id = 1; id <= board.players.length; id++)
		renderPlayer(id, board.players[id-1],board.usernames[id-1]);
	for (var id = 1; id <= 3; id++) renderTokens(id, board.suspects[id-1]);
}

/* Render players. This function is called by renderBoard(). */
function renderPlayer(id, player,username) {
	$('#player'+id).css("display", "inline-block");
	$('p.player').css('display', 'block');
	if(username == "")
		$('#username'+id).text('Waiting Player '+id);
	else
		$('#username'+id).text(username)
	$('#accToken'+id).text('x '+player.accTokens);
	$('#blackToken'+id).text('x '+player.blackTokens);
	if(player.observer)
		$('#observer'+id).show();
	else
		$('#observer'+id).hide();
}

/* Render alibis. This function is called by renderBoard(). */
function renderAlibi(id, value) {
	if(value > 0)
		$('#alibi'+id).text(value);
	else if(value == 0)
		$('#alibi'+id).text('B');
	else
		$('#alibi'+id).html('&nbsp;');
	$('#alibi'+id).css("display", "inline-block");
}

/* Render victim. This function is called by renderBoard(). */
function renderVictim(value) {
	if(value > 0)
		$('#victim').text(value);
	else if(value == 0)
		$('#victim').text('B');
	else
		$('#victim').html('&nbsp;');
	$('#victim').css("display", "inline-block");
}

/* Render suspects. This function is called by renderBoard(). */
function renderSuspect(id, suspect) {
	if(suspect.value > 0)
		$('#suspect'+id).text(suspect.value);
	else if(suspect.value == 0)
		$('#suspect'+id).text('B');
	else
		$('#suspect'+id).html('&nbsp;');
	$('#suspect'+id).css("display", "inline-block");
	renderTokens(id, suspect);
}

function renderTokens(id, suspect) {
	if(suspect.unseenToken)
		$('#unseen'+id).css("visibility", "visible");
	else
		$('#unseen'+id).css("visibility", "hidden");
	if(suspect.swapToken)
		$('#swap'+id).css("visibility", "visible");
	else
		$('#swap'+id).css("visibility", "hidden");
	var color = {1: 'blue', 2: 'green', 3: 'red', 4: 'yellow'};
	for(var k = 1; k <= suspect.accTokens.length; k++)
		$('#tb'+k+id).css("background-image", "url(static/img/token_"+color[suspect.accTokens[k-1]]+".png)");
	if(suspect.accTokens.length == 0) {
		for(var m = 1; m <= 4; m++) 
			$('#tb'+m+id).css("background-image", "none");
	}
}

/* Hide the gameboard. */
function hideGameBoard() {
	$('div.gameboard').hide();
}
/* Shows the available rooms */
function showRooms() {
	$('#rooms').show();
	$('.btn.btn-success.action.join').show();
	getRooms();
}

function hideRooms() {
	$('#rooms').hide();
	$('.btn.btn-success.action.join').hide();	
}
/* Highlights which player's turn it is */
function highLightPlayers(json) {
	for(var id = 1; id <= json.players.length; id++) {
		if(json.highlight.indexOf(id) > -1)
			$('#player'+id).css('border-width', '4px');
		else
			$('#player'+id).css('border-width', '0px');
	}
}

function removeHighLightPlayer() {
	for(var i = 1; i <= 4; i++)
		$('#player'+i).css('border-width', '0px');
}


