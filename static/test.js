var fixture = $( "#qunit-fixture" );
var suspectsChosen = [];
var gameId = null;
var playerId = null;
/* Test unit for hideDescription() */
test("Hide Description", function() {
	hideDescription();
	equal($('#description').html(), '<br>', 'Description is hidden by changing content to <br>.');
});

/* Test unit for hideActions() */
test("Hide Actions", function() {
	/* Add Action buttons. */
	actions = { '#start': 'Start New Game',
				'#checkAlibi': 'Check Alibi',
				'#nextAlibi': 'Next',
				'#rotateAlibis': 'Rotate Alibis',
				'#chooseSuspects': 'Choose Suspects',
				'#checkSuspects': 'Check Suspects',
				'#next': 'Next',
				'#placeToken': 'Place Token',
				'#swapSuspect': 'Swap Suspect',
				'#showResult': 'Show Result',
				'#returnTokens': 'Return Tokens',
				'#nextRound': 'Next Round',
				'#2': '2 Players',
				'#3': '3 Players',
				'#4': '4 Players',
				'#yes': 'Yes',
				'#no': 'No'};
	hideActions();
	for(var id in actions)
		equal($(id).is(':visible'), false, "Button "+actions[id]+" is hidden.");
});

/* Test unit for howManyPlayers() */
test("How Many Players", function() {
	howManyPlayers();
	equal($('#description').text(), "How many players?", 'Description shows successfully.');
	for(var i = 2; i <= 4; i++)
		equal($('#'+i).is(':visible'), true, "Buttons show successfully.");
});

/* Test unit for startGame() */
test("Start Game", function(assert) {
	var done = assert.async();
	var start = startGame(4); // return a function.
	start();
	setTimeout(function() {
		equal($('#checkAlibi').is(':visible'), true, "Check Alibi button shows successfully.");
		ok(gameId != null, "Game is initialized successfully.");
		equal(playerId, 1, "Player ID is initialized successfully.");
		equal($('div.gameboard').is(':visible'), true, "Game board shows successfully.");
		done();
	}, 100);
});

/* Test unit for clearSuspectsChosen() */
test("Chear suspectsChosen list", function() {
	suspectsChosen.length = 0;
	suspectsChosen.push(1);
	suspectsChosen.push(2);
	clearSuspectsChosen();
	equal(suspectsChosen.length, 0, "List suspectsChosen is cleard.");
});

/* Test unit for choosenSuspectHandler() */
test("Choosen Suspect Handler", function() {
	suspectsChosen.length = 0;
	chooseSuspectHandler(2, suspectsChosen)();
	equal(suspectsChosen[0], 2, "Add ingeter 2 to list suspectsChosen successfully.");
	equal($('#chooseSuspect2').is(':visible'), false, "Green button for suspect 2 is hidden.");
	equal($('#removeSuspect2').is(':visible'), true, "Blue button for suspect 2 is shown.");
});

/* Test unit for removeSuspectHandler() */
test("Remove Suspect Handler", function() {
	suspectsChosen = [2, 3];
	removeSuspectHandler(2, suspectsChosen)();
	ok(suspectsChosen[0] == 3 && suspectsChosen.length == 1, "Integer 2 is removed from list suspectsChosen successfully.");
	equal($('#chooseSuspect2').is(':visible'), true, "Green button for suspect 2 is shown.");
	equal($('#removeSuspect2').is(':visible'), false, "Blue button for suspect 2 is hidden.");
});

/* Test unit for renderPlayer() */
test("Render Player", function() {
	var id = 3;
	var player = {'id': id, 'accTokens': 3, 'blackTokens': 2, 'observer': true};
	renderPlayer(id, player);
	equal($('#player'+id).is(':visible'), true, "Player 3 is rendered successfully.");
	equal($('#accToken'+id).is(':visible'), true, "The accusation tokens of Player 3 is renderd successfully.");
	equal($('#accToken'+id).text(), 'x 3', "The number of accusation tokens of Players is rendered correctly.");
	equal($('#blackToken'+id).text(), 'x 2', "The number of black tokens of Players is rendered correctly.");
	equal($('#observer'+id).is(':visible'), true, "Observer token is rendered correctly.");
});

/* Test unit for renderAlibi() */
test("Render Alibi", function() {
	var ids = [1, 3, 4];
	var values = [0, -1, 5];
	renderAlibi(ids[0], values[0]);
	renderAlibi(ids[1], values[1]);
	renderAlibi(ids[2], values[2]);
	for(var i = 0; i < 3; i++)
		equal($('#alibi'+ids[i]).is(':visible'), true, "Alibi "+ids[i]+" is rendered successfully.");
	equal($('#alibi'+ids[0]).text(), 'B', "The value of alibi "+ids[0]+" is rendered corerctly");
	equal($('#alibi'+ids[1]).html(), '&nbsp;', "The value of alibi "+ids[1]+" is rendered corerctly");
	equal($('#alibi'+ids[2]).text(), '5', "The value of alibi "+ids[2]+" is rendered corerctly");
});

/* Test unit for renderVictim() */
test("Render Victim", function() {
	renderAlibi(3);
	equal($('#victim').is(':visible'), true, "Victim is rendered successfully.");
});

/* Test unit for renderVictim() */
test("Render Suspecgt", function() {
	var id = 2;
	var suspect = {value: 5, accTokens: [2, 3], swapToken: true, unseenToken: true, murderer: true};
	renderSuspect(id, suspect);
	equal($('#suspect'+id).is(':visible'), true, "Suspect 2 is shown successfully.");
	equal($('#suspect'+id).text(), '5', "Suspect 2 is shown successfully.");
	equal($('#unseen'+id).is(':visible'), true, "Unseen token for suspect 2 is shown successfully.");
	equal($('#swap'+id).is(':visible'), true, "Swap token for suspect 2 is shown successfully.");
	equal($('#tb12').is(':visible'), true, "Placed token 1 on suspect 2 is shown successfully.");
	equal($('#tb22').is(':visible'), true, "Placed token 2 on suspect 2 is shown successfully.");
});

/* Test unit for hideGameBoard() */
test("Hide Game Board", function() {
	hideGameBoard();
	equal($('div.gameboard').is(':visible'), false, "Game Board is hidden successfully.");
});