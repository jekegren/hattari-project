var suspectsChosen = [];
var gameId=null;
var playerId = 1;
$(function() {

	/* Action for "Start Game" button. */
	$('#start').click(function() {
		hideActions();
		howManyPlayers();
		hideGameBoard();
	});

	/* Action for 2-4 Players buttons. */
	for (var num = 2; num <= 4; num++) 
		$('#'+num).click(startGame(num));

	for (var id = 1; id <= 3; id++) {
		/* Action for green "Suspect" buttons. */
		$('#chooseSuspect'+id).click(chooseSuspectHandler(id, suspectsChosen));
		/* Action for blue "Suspect" buttons. */
		$('#removeSuspect'+id).click(removeSuspectHandler(id, suspectsChosen));
	}
})

/* Hide the description. */
function hideDescription() {
	$('#description').html('<br>');
}

/* Hide all actions buttons. */
function hideActions() {
	$('.btn.btn-success.action').hide();
}

/* Hide board. */
function hideBoard() {
	$.getJSON("/game/hideboard/"+gameId, function(json) {renderBoard(json)});
}

function getMentalState() {
    $('#well_text').text('Computing...');
    $.getJSON("/game/mentalstate/"+gameId, function(res) {
        $('#well_text').text(res.state);
    });
}

/* Ask player how many players. */
function howManyPlayers() {
	$('#description').text("How many players?").hide().fadeIn();
	for (var i = 2; i <= 4; i++) $('#'+i).fadeIn();
}

/* Start game with 'numPlayers' players. */
function startGame(numPlayers) {
	return function() {
		$.getJSON("/game/start/"+numPlayers, function(json) {
			renderBoard(json);
			hover(json);
			$('#mentalstate').show();
			hideActions();
			hideDescription();
			$('div.tokenTop').css('visibility', 'hidden');
			$('#checkAlibi').on('click',function(){
				$('#checkAlibi').off();
				checkAlibi();});
			$('#checkAlibi').fadeIn();
			gameId=json.gameId;
			playerId = json.currTurn;
			$('div.gameboard').css('opacity', '1.0');
			$('div.gameboard').hide().fadeIn(750);
			removeHighLightPlayer();
			highLightPlayer(1);
		});
	}
}

/* Action for "Check Alibi" button. */
function checkAlibi() {
	$.getJSON("/game/checkalibi/"+playerId+'/'+gameId, function(json) {
		hover(json);
		$('#alibi'+playerId).fadeOut(750, function() {
			renderBoard(json);
			$('#alibi'+playerId).hide().fadeIn(750);
			hideActions();
			$('#nextAlibi').on('click',function(){
				$('#nextAlibi').off();
				nextAlibi();});
			$('#nextAlibi').fadeIn();
			playerId = json.currTurn;
		});
	});
}

/* Action for "Next" button. */
function nextAlibi() {
	$.getJSON("/game/hideboard/"+gameId, function(json) {
		hover(json);
		if(playerId == 1) i = json.players.length;
		else i = playerId - 1;
		$('#alibi'+i).fadeOut(750, function() {
			renderBoard(json);
			$('#alibi'+i).hide().fadeIn(750);
			hideActions();
			if (json.stage == 1 || json.stage == 3) {
				$('#checkAlibi').on('click',function(){
				$('#checkAlibi').off();
				checkAlibi();});
				$('#checkAlibi').fadeIn();
				removeHighLightPlayer();
				highLightPlayer(playerId);
			}
			else if (json.stage == 2) {
				$('#rotateAlibis').on('click',function(){
				$('#rotateAlibis').off();
				rotateAlibis();});
				$('#rotateAlibis').fadeIn();
				removeHighLightPlayer();
			}
			else if (json.stage == 4) {
				$('#chooseSuspects').on('click',function(){
				$('#chooseSuspects').off();
				chooseSuspects();});
				$('#chooseSuspects').fadeIn();
				$('.btn.btn-success.choose-suspect').css('display', 'inline-block');
				$('#description').text('Please choose 2 suspects to check.').hide().fadeIn();
				removeHighLightPlayer();
				highLightPlayer(playerId);
			}
		});
	});
}

/* Action for "Rotate Alibis" button. */
function rotateAlibis() {
	$.getJSON("/game/rotatealibis/"+gameId, function(json) {
		hover(json);
		numPlayers = json.players.length;
		$('#alibi1').animate({right: "-="+(242.5*(numPlayers-1))+"px"}, {queue: false, complete: function() {
			$('#alibi1').animate({right: "+="+(242.5*(numPlayers-1))+"px"}, 0);
		}});
		for(var id = 2; id <= numPlayers; id++) {
			$('#alibi'+id).animate({left: "-=242.5px"}, {queue: false, complete: function() {
				$(this).animate({left: "+=242.5px"}, 0);
			}});
		}
		renderBoard(json);
		hideActions();
		$('#checkAlibi').on('click',function(){
			$('#checkAlibi').off();
				checkAlibi();});
		$('#checkAlibi').fadeIn();
		removeHighLightPlayer();
		highLightPlayer(json.currTurn);
	})
}

/* Action for "Choose suspects" button. */
function chooseSuspects() {
	if (suspectsChosen.length != 2) {
		$('#chooseSuspects').on('click',function(){ $('#chooseSuspects').off(); chooseSuspects();});
		return toastr.warning('Please choose 2 suspects.');}
	$.getJSON("/game/choosesuspects/"+suspectsChosen[0]+'&'+suspectsChosen[1]+'&'+gameId, function(json) {
		$('#suspect'+suspectsChosen[0]+', #suspect'+suspectsChosen[1]).fadeOut(750, function() {
			renderBoard(json);
			$('#description').text('Do you want to swap one suspect with the victim?').hide().fadeIn();
			hideActions();
			$('#yes').on('click',function(){
				$('#yes').off();
				yes();});
			$('#yes').fadeIn();
			$('#no').on('click',function(){
				$('#yno').off();
				no();});
			$('#no').fadeIn();
		});
	});
}

/* Action for "Yes" button. */
function yes() {
	hideActions();
	clearSuspectsChosen();
	$('#swapSuspect').off();
	$('#swapSuspect').on('click',function(){
		$('#swapSuspect').off();
		swapSuspect();});
	$('#swapSuspect').fadeIn();
	$('#description').text('Please choose 1 suspect to swap with the victim.').hide().fadeIn();
	$('.btn.btn-primary.choose-suspect').hide();
	$('.btn.btn-success.choose-suspect').fadeIn();
}

/* Action for "No" button. */
function no() {
	hideActions();
	clearSuspectsChosen();
	$('#placeToken').off();
	$('#placeToken').on('click',function(){$('#placeToken').off();placeToken();});
	$('#placeToken').fadeIn();
	$('#description').text('Please choose 1 suspect to place token.').hide().fadeIn();
	$('.btn.btn-primary.choose-suspect').hide();
	$('.btn.btn-success.choose-suspect').fadeIn();
}

/* Action for "Swap Suspect" button. */
function swapSuspect() {
	if (suspectsChosen. length != 1) {
		$('#swapSuspect').off();
		$('#swapSuspect').on('click',function(){
		$('#swapSuspect').off();
		swapSuspect();});
		return toastr.warning('Please choose 1 suspect.');
	}
	$.getJSON("/game/swapsuspect/"+suspectsChosen[0]+'&'+gameId, function(json) {
		$('#victim, #suspect'+suspectsChosen[0]).fadeOut(750, function() {
			renderBoard(json);
			$('#victim, #suspect'+suspectsChosen[0]).fadeIn(750);
			hideActions();
			clearSuspectsChosen();
			$('#placeToken').off();
			$('#placeToken').on('click',function(){
			$('#placeToken').off();
			placeToken();});
			$('#placeToken').fadeIn();
			$('#description').text('Please choose 1 suspect to place token.').hide().fadeIn();
			$('.btn.btn-primary.choose-suspect').hide();
			$('.btn.btn-success.choose-suspect').fadeIn();
		});
	});
}

/* Action for "Check suspects" button. */
function checkSuspects() {
	$.getJSON("/game/checksuspects/"+gameId, function(json) {
		hover(json);
		var s = [];
		for(var i = 1; i <= json.suspects.length; i++) {
			if(json.suspects[i-1].value >= 0)
				s.push(i);
		}
		$('#suspect'+s[0]+', #suspect'+s[1]).fadeOut(750, function() {
			renderBoard(json);
			hideActions();
			clearSuspectsChosen();
			$('#placeToken').off();
			$('#placeToken').on('click',function(){
				$('#placeToken').off();
				placeToken();});
			$('#placeToken').fadeIn();
			$('#description').text('Please choose 1 suspect to place token.').hide().fadeIn();
			$('.btn.btn-primary.choose-suspect').hide();
			$('.btn.btn-success.choose-suspect').fadeIn();
		});
	});
}

/* Action for "Place token" button. */
function placeToken() {
	if (suspectsChosen.length != 1) {
		$('#placeToken').off();
		$('#placeToken').on('click', function () {
			$('#placeToken').off();
			placeToken();
		});
		return toastr.warning('Please choose 1 suspect to place token.');
	}
	$.getJSON("/game/placetoken/"+suspectsChosen[0]+'&'+gameId, function(json) {
		hover(json);
		var s = [];
		for(var i = 1; i <= json.suspects.length; i++) {
			if(json.suspects[i-1].value >= 0)
				s.push(i);
		}
		$('#suspect'+s[0]+', #suspect'+s[1]).fadeOut(750, function() {
			renderBoard(json);
			hideActions();
			clearSuspectsChosen();
			hideDescription();
			hideBoard();
			$('.btn.btn-primary.choose-suspect').hide();
			$('.btn.btn-success.choose-suspect').hide();
			removeHighLightPlayer();
			if (json.stage == 4) {
				$('#checkSuspects').off();
				$('#checkSuspects').on('click',function(){
				$('#checkSuspects').off();
				checkSuspects();});
				$('#checkSuspects').fadeIn();
				highLightPlayer(json.currTurn);
			}
			else if (json.stage == 6) {
				$('#showResult').off();
			$('#showResult').on('click',function(){
				$('#showResult').off();
				showResult();});
				$('#showResult').fadeIn();
		}
	});
});
}
/* Action for "Show result" button. */
function showResult() {
	$.getJSON("/game/showresult/"+gameId, function(json) {
		for (var i = 0; i < 3; i++) {
			if(json.suspects[i].murderer) {
				$('#description').text('The murderer is ' + json.suspects[i].value).hide().fadeIn();
				$('#suspect'+(i+1)).css('color', 'red');
			}
		}
		$('#alibis, #victim, #suspects').fadeOut(750, function() {
			renderBoard(json);
			$('#alibis, #victim, #suspects').show();
			hideActions();
			$('#returnTokens').off();
			$('#returnTokens').on('click',function(){
				$('#returnTokens').off();
				returnTokens();});
			$('#returnTokens').fadeIn();

		});

	});
}

/* Action for "Return Tokens" button. */
function returnTokens() {
	$.getJSON("/game/returntokens/"+gameId, function(json) {
		renderBoard(json);
		hideActions();
		hideDescription();
		$('.tokenBottom').css('background-image', 'none');
		$.getJSON("/game/gameovercheck/"+gameId, function(json) {
			if (!json.gameover) {
				$('#nextRound').off();
			$('#nextRound').on('click',function(){
				$('#nextRound').off();
				nextRound();});
				$('#nextRound').fadeIn();}
			else {
				msg = 'GAME OVER. Loser:';
				for (var i = 0; i < json.loserIds.length; i++)
					msg += ' Player ' + (json.loserIds[i]);
				$('#description').text(msg).hide().fadeIn();
				$('#start').fadeIn();
				$('div.gameboard').fadeTo(750, 0.4);
			}
		});
	});
}

/* Action for "Next Round" button. */
function nextRound() {
	$.getJSON("/game/newround/"+gameId, function(json) {
		renderBoard(json);
		hideActions();
		$('#checkAlibi').on('click',function(){
				$('#checkAlibi').off();
				checkAlibi();});
		$('#checkAlibi').fadeIn();
		highLightPlayer(json.currTurn)
		playerId = json.currTurn;
		$('div.suspect').css('color', '#ffffff');
	})
}

/* Clear suspectsChosen list. */
function clearSuspectsChosen() {
	suspectsChosen.length = 0;
}

/* Action for green "Suspect" buttons. */
function chooseSuspectHandler(id, suspectsChosen) {
	return function() {
		suspectsChosen.push(id);
		$('#chooseSuspect'+id).hide();
		$('#removeSuspect'+id).show();
	}
}

/* Action for blue "Suspect" buttons. */
function removeSuspectHandler(id, suspectsChosen) {
	return function() {
		index = suspectsChosen.indexOf(id);
		if (index >= 0) {
			suspectsChosen.splice(index, 1);
			$('#chooseSuspect'+id).show();
			$('#removeSuspect'+id).hide();
		}
	}
}

/* Render the board. */
function renderBoard(board) {
	for (var id = 1; id <= board.players.length; id++) {
		renderPlayer(id, board.players[id-1]);
		renderAlibi(id, board.alibis[id-1]);
	}
	renderVictim(board['victim'])
	for (var id = 1; id <= 3; id++) renderSuspect(id, board.suspects[id-1])
}

/* Render players. This function is called by renderBoard(). */
function renderPlayer(id, player) {
	$('#player'+id).css("display", "inline-block");
	$('#accToken'+id).text('x '+player.accTokens);
	$('#blackToken'+id).text('x '+player.blackTokens);
	if(player.observer)
		$('#observer'+id).show();
	else
		$('#observer'+id).hide();
}

/* Render alibis. This function is called by renderBoard(). */
function renderAlibi(id, value) {
	if(value > 0)
		$('#alibi'+id).text(value);
	else if(value == 0)
		$('#alibi'+id).text('B');
	else
		$('#alibi'+id).html('&nbsp;');
	$('#alibi'+id).css("display", "inline-block");
}

/* Render victim. This function is called by renderBoard(). */
function renderVictim(value) {
	if(value > 0)
		$('#victim').text(value);
	else if(value == 0)
		$('#victim').text('B');
	else
		$('#victim').html('&nbsp;');
	$('#victim').css("display", "inline-block");
}

/* Render suspects. This function is called by renderBoard(). */
function renderSuspect(id, suspect) {
	if(suspect.value > 0)
		$('#suspect'+id).text(suspect.value);
	else if(suspect.value == 0)
		$('#suspect'+id).text('B');
	else
		$('#suspect'+id).html('&nbsp;');
	$('#suspect'+id).css("display", "inline-block");
	if(suspect.unseenToken)
		$('#unseen'+id).css("visibility", "visible");
	else
		$('#unseen'+id).css("visibility", "hidden");
	if(suspect.swapToken)
		$('#swap'+id).css("visibility", "visible");
	else
		$('#swap'+id).css("visibility", "hidden");
	var color = {1: 'blue', 2: 'green', 3: 'red', 4: 'yellow'};
	for(var k = 1; k <= suspect.accTokens.length; k++)
		$('#tb'+k+id).css("background-image", "url(static/img/token_"+color[suspect.accTokens[k-1]]+".png)");
	if(suspect.accTokens.length == 0) {
		for(var m = 1; m <= 4; m++) 
			$('#tb'+m+id).css("background-image", "none");
	}
}

function hover(board) {
	/*for (var id=1; id <= 4; id++) {
		hovering(board,id)
	}*/
}

function hovering(board,id){
	$('#player'+id).click(function(){
		var hovertext=board.info[id-1];
		$('#well').show();
		$('#well_text').text(hovertext);
});}

/* Hide the gameboard. */
function hideGameBoard() {
	$('div.gameboard').hide();
}

function highLightPlayer(playerId) {
	$('#player'+playerId).css('border-width', '4px');
}

function removeHighLightPlayer() {
	for(var i = 1; i <= 4; i++)
		$('#player'+i).css('border-width', '0px');
}

